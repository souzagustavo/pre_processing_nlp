from model.review import Review
from pymongo import MongoClient
import os

# cria conexão com banco MongoDB
def connect():
    host = os.getenv('DB_HOST')
    user = os.getenv('DB_USER')
    password = os.getenv('DB_PASSWORD')
    return MongoClient(f"mongodb+srv://{user}:{password}@{host}?retryWrites=true&w=majority")

# adiciona documento de review ao mongoDb, se documento já existir é atualizar
def insertReview(client, id, source, review):
    return getReviewsCollection(client).update_one({'id': id, 'source': source}, { '$set': review , '$unset': { 'author': "" }}, upsert=True)

# busca review através do id
def findById(client, id):
    cursor = getReviewsCollection(client).find({'id': id}).limit(1)
    reviews = []
    for document in cursor:
        reviews.append(parseReview(document))
    return reviews

# verifica se existe review do id especificado
def containsById(client, id):
    reviews =  findById(client, id)
    return len(reviews) > 0

# retorna a collection onde se encontram os reviews
def getReviewsCollection(client):
    database = client["univali"]
    return database["reviews"]

# transforma o documento retornado pelo mongoDB para o modelo Review
def parseReview(document):
    return Review(document['id'], document['timestamp'], document['author'], document['rawText'], document['processedText'], document['game'], document['source'], document['polarity']) 
