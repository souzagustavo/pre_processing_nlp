#Estrutura de dados na qual os reviews serão armazenados no MongoDB
class Review:
    def __init__(self, id, timestamp, rawtext, processedText, game, source, polarity, polarityDescription):
        self.id = id
        self.rawText = rawtext
        self.polarity = polarity
        self.game = game.title()
        self.source = source.title()
        self.processedText = processedText
        self.polarity = polarity
        self.timestamp = timestamp
        self.polarityDescription = polarityDescription

