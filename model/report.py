import statistics

class Report:
    def __init__(self, absPath):
        self.absPath     = absPath
        self.lines       = 0
        self.positive    = 0
        self.negative    = 0
        self.errors      = 0
        self.neutral     = 0
        self.empty       = 0

        self.pre_processing_ms = list()
        self.insert_ms = list()

    def add_error(self):
        self.errors += 1
    
    def add_positive(self):
        self.positive += 1

    def add_negative(self):
        self.negative += 1

    def add_neutral(self):
        self.neutral += 1

    def add_lines(self):
        self.lines += 1

    def add_empty(self):
        self.empty += 1

    def get_avg_processing(self):
        if len(self.pre_processing_ms) > 0:
            return statistics.fmean(self.pre_processing_ms)
        else:
            return 0

    def get_avg_insert(self):
        if len(self.insert_ms) > 0:
            return statistics.fmean(self.insert_ms)
        else:
            return 0

    def show(self):
        print("Relatório de importação: ", self.absPath, " \nLines:", self.lines, " \nPositive: ", self.positive, " \nNegative: ", self.negative, " \nNeutro: ", self.neutral, " \nVazio: ", self.empty, " \nErros: ", self.errors)
        print("Pre processing ms AVG: ", self.get_avg_processing())
        print("Insert ms AVG: ", self.get_avg_insert())
        print("\n*****************************************************\n")