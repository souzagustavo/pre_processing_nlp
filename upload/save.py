import os
import statistics
import sys
import time
from datetime import datetime
from model.report import Report

from nlp.pre_processing import lgpd, preProcessing
sys.stdout.reconfigure(encoding='utf-8')

from database.db import connect, containsById, insertReview

from model.review import Review

# lista de arquivos do path
def getFiles(path):
    return os.listdir(path)

# nome do arquivo sem extensão
def getFileNameWithoutExtension(fileName):
    fileName = fileName.split('.')[0] 
    fileName = fileName.replace('-', ' ')
    return fileName

# importa arquivo CSV de reviews para MongoDB
def importFile(source, path, fileName):
    absPath = path + "\\" + fileName

    report = Report(absPath)
    # arquivos com final -id.csv -backup.csv não devem ser importados
    if '-id.csv' in fileName or '-backup.csv' in fileName:
        return report
    
    #inicia conexão com MongoDB
    client = connect()
    print("Abrindo arquivo: ", absPath)
    with open(absPath, 'rt', encoding="utf8", errors="surrogateescape") as file:
    
        while line := file.readline():
            report.add_lines()
            row = line.rstrip().split(';')

            #verifica se linha possui 5 colunas (id;timestamp;author;text;polarity) ou 4 colunas (id;timestamp;author;text)
            if len(row) != 5 and len(row) != 4:
                report.add_error()
                continue

            #author      = row[2]
            id          = row[0]
            timestamp   = row[1]
            rawText     = row[3]
            polarity    = ''
            polarity_desc = ''

            if len(row) == 5:
                polarity = row[4]

            #verifica se data, texto  estão vazios
            if id == '' or timestamp == '' or rawText == '':
                report.add_error()
                continue

            rawText = lgpd(rawText)

            start_pre = time.time()
            processedText = preProcessing(rawText)
            end_pre = time.time()

            report.pre_processing_ms.append((end_pre - start_pre) * 1000)
            #verifica se texto processado fica vazio
            if processedText == '':
                report.add_empty()
                continue

            if len(row) == 4:
                polarity_bool = None
                polarity_desc = 'no_label'
            elif polarity == 'true':
                polarity_bool = True
                polarity_desc = 'positive'
                report.add_positive()
            elif polarity == 'false':
                polarity_bool = False
                polarity_desc = 'negative'
                report.add_negative()
            else:
                polarity_bool = None
                polarity_desc = 'neutral'
                report.add_neutral()

            review = createReview(source, getFileNameWithoutExtension(fileName), id, timestamp, rawText, processedText, polarity_bool, polarity_desc)
            # insere ou atualiza registro no banco
            start_insert = time.time()
            insertReview(client, review.id, review.source, review.__dict__)
            end_insert = time.time()

            report.insert_ms.append((end_insert - start_insert) * 1000)

        file.close()

        report.show()
        return report

# cada plataforma utiliza uma formatação da data diferente, portanto deve ser tratada separadamente
def createReview(source, game, id, timestamp, raw_text, processed_text, polarity, polarity_desc):
    if source == 'twitter':
        return Review(id, format_twitter_date(timestamp), raw_text, processed_text, game, source, polarity, polarity_desc)
    elif source == 'steam':
        return Review(id, format_steam_date(int(timestamp)), raw_text, processed_text, game, source, polarity, polarity_desc)

# formata data do twitter para ISO
def format_twitter_date(timestamp):
    return datetime.strptime(timestamp, '%a %b %d %H:%M:%S %z %Y').isoformat()

# formata data da Steam para ISO
def format_steam_date(timestamp):
    return datetime.fromtimestamp(timestamp).isoformat()