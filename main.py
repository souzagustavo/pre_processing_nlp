from model.report import Report
from upload.save import getFiles, importFile
from dotenv import load_dotenv
import os

# importa arquivos CSV relacionados ao Twitter para MongoDB
def importTwitterReviews():
    folder_reviews_twitter = os.getenv("FOLDER_REVIEWS_TWITTER")
    files = getFiles(folder_reviews_twitter)
    
    reports = list()
    for f in files:
        reports.append( importFile('twitter', folder_reviews_twitter, f) )
    show_overall_report("Twitter", reports)

# importa arquivos CSV relacionados a Steam para MongoDB
def importSteamReviews():
    folder_reviews_steam = os.getenv("FOLDER_REVIEWS_STEAM")
    files = getFiles(folder_reviews_steam)

    reports = list()
    for f in files:
        reports.append( importFile('steam', folder_reviews_steam, f) )
    show_overall_report("Steam", reports)

def show_overall_report(source, reports):
    final_report = Report(source)

    for report in reports:
        final_report.lines += report.lines
        final_report.positive += report.positive
        final_report.negative += report.negative
        final_report.neutral += report.neutral
        final_report.empty += report.empty

        avg_pro = report.get_avg_processing()
        avg_insert = report.get_avg_insert()

        if avg_pro > 0:
            final_report.pre_processing_ms.append(avg_pro)
        if avg_insert > 0:            
            final_report.insert_ms.append(avg_insert)

    final_report.show()


if __name__ == '__main__':
    load_dotenv()

    importTwitterReviews()
    importSteamReviews()