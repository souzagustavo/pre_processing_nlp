# Pré-processamento

Script responsável por pré-processar e importar os arquivos CSV rotulados para banco de dados MongoDB

## Pré-requisitos
* Cadastrar usuário/senha para conectar ao MongoDB
* Cadastrar IP's permitidos para conectar ao MongoDB
* Definir pasta onde estão arquivos do Twitter e Steam

## Relatório de importação 2022-03-23:
```
Twitter:
  Lines: 33221  | Positivo/Negativo:  3175  | Neutro:  9933  | Vazio:  0
Steam:
  Lines: 68870  | Positivo/Negativo:  65169  | Neutro:  0  | Vazio:  3609
```

Add file `.env` with Environment variables to root folder:
```
DB_HOST=sentiment-analysis-gami.nh2gy.mongodb.net/univali
DB_USER=<VALUE>
DB_PASSWORD=<VALUE>
FOLDER_REVIEWS_TWITTER=<PATH>
FOLDER_REVIEWS_STEAM=<PATH>
```

How to run:
```
py main.py
```


