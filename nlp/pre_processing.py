import nltk
nltk.download('stopwords')

import re
import unidecode


def preProcessing(text):

    text = lowerCase(text)
    text = removeLinks(text)
    text = removeHashtag(text)
    text = removeMention(text)
    text = removeDigits(text)
    text = removePunctation(text)
    text = removeStopwords(text)
    text = removeAccents(text)
    text = correctSpelling(text)
    text = removeShortTokens(text)
    #print('post: ' +text + "\n*********************************\n")
    return text

def lgpd(text):
    text = maskMention(text)
    return text

# remove acentos
def removeAccents(text):
    return unidecode.unidecode(text).strip()

# remove tokens de tamanho igual à 1
def removeShortTokens(text):
    tokens = text.split()
    cleanTokens = [t for t in tokens if len(t) > 1]
    return ' '.join(cleanTokens).strip()

# converte texto para minúsculo
def lowerCase(text):
    return text.lower().strip()

# remove pontuações
def removePunctation(text):
    tokenizer = nltk.RegexpTokenizer(r"\w+")
    newText = tokenizer.tokenize(text)
    return ' '.join(newText).strip()

# remove stopwords utilizando NLTK
def removeStopwords(text):
    new_text = []
    stopwords = nltk.corpus.stopwords.words('portuguese')
    for word in text.split():
        if word not in stopwords:
            new_text.append(word)
    return ' '.join(new_text).strip()

#remove referências
def removeLinks(text):
    return re.sub(r"http\S+", ' ', text).strip()

#remove palavras
def removeHashtag(text):
    return re.sub(r"#(\w+)", ' ', text).strip()

# remove menções iniciadas com @
def removeMention(text):
    return re.sub(r"@(\w+)", ' ', text).strip()

# substitui menções iniciadas com @ por @usuario
def maskMention(text):
    return re.sub(r"@(\w+)", '@usuario', text).strip()

# remove digitos da frase
def removeDigits(text):
    return re.sub(r"\d", ' ', text).strip()

# correção de palavras
def correctSpelling(text):
    new_text = []
    for word in text.split():
        if word == 'n': #nope nopes
            new_text.append(word.replace('n','nao'))
        elif word == 'nn':
            new_text.append(word.replace('nn','nao'))
        else:
            new_text.append(word)
    return ' '.join(new_text).strip()


